#!groovy

properties([disableConcurrentBuilds()])

pipeline {
    agent { 
        label 'master'
        }
    options {
        buildDiscarder(logRotator(numToKeepStr: '10', artifactNumToKeepStr: '10'))
        timestamps()
    }
    stages {
        stage("run sonarqube image") {
            steps {
                sh "docker run -d --name sonarqube -p 8082:9000 sonarqube"
            }
        }
        stage("run nexus image") {
            steps {
                sh "docker run -d -p 8083:8081 --name nexus sonatype/nexus3"
            }
        }
    }
}
