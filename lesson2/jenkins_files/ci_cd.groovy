#!groovy

pipeline {
    options {
        timestamps()
    }
    agent {
        label 'master'
    }
    stages {
        stage('git checkout') {
            steps {
                git credentialsId: 'bitbucket', url: 'git@bitbucket.org:ashuikam/develop_test.git'
            }
        }
        stage('package &sonarQube analysis') {
            steps {
                withSonarQubeEnv('epam_sq') {
                    sh "mvn clean package sonar:sonar"
                }
            }
        }
        stage('nexus upload') {
            steps {
                nexusArtifactUploader artifacts: [[artifactId: 'hello',
                                                   classifier: '',
                                                   file: 'target/hello.war',
                                                   type: 'war']],
                                      credentialsId: 'nexus_crd',
                                      groupId: 'com.hello',
                                      nexusUrl: '34.65.178.147:8083',
                                      nexusVersion: 'nexus3',
                                      protocol: 'http',
                                      repository: 'maven-releases',
                                      version: "${BUILD_NUMBER}-${BUILD_TIMESTAMP}"
            }
        }
        stage('deploy') {
            when {
                expression { return currentBuild.number%3 == 0 }
            }
            steps {
                sh 'cp target/hello.war /opt/tomcat/webapps/'
            }
        }
    }
}
