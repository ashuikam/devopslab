#!env/bin/python3

# coded by Mikalai Ashuika
# e-mail ashuikam@gmail.com
#
# The scripts gets weather from openweathermap.org
# and prints or sends it to emails

import os

import smtplib
import ssl

import requests
import argparse

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from tabulate import tabulate
from validate_email import validate_email


class ParseWeather:
    """The class ParserWeather

    class properties:
    self.request instance of class requests.Session
    self.url (default <str> http://api.openweathermap.org/data/2.5/weather?q=)
    self.emails (!!!default <boolean> False) - <list>
    self.get (default empty) - <str>
    self.city (default empty) - <str>
    methods:
    public method: getArgs(args)
    public method: goWeather()

    """
    def __init__(self):
        """Constructor

        The method sets the default values for class properties,
        and creates (propertie self.request) instance of class
        requests.Session

        """
        self.request = requests.Session()
        self.url = "http://api.openweathermap.org/data/2.5/weather?q="
        self.emails = False
        self.get = ""
        self.city = ""

    def _getWeather(self):
        """The protected method returns response from openweathermap <dict>"""
        try:
            response = self.request.get(self.url + self.get)
            if response.status_code == 200:
                return response.json()
            else:
                raise Exception("No server's data, response: %s"
                                % response.json())
        except Exception as error:
            print(error)
            return False

    def _setDetails(self):
        """The protected method returns env vars:

        arguments:
        GMAIL_USER - <str>
        GMAIL_PASSWORD <str>

        return:
        sender email - <str>
        password - <str>

        """
        try:
            sender_email = False
            password = False
            if 'GMAIL_USER' in os.environ:
                sender_email = os.environ.get('GMAIL_USER')
            else:
                raise Exception("No 'GMAIL_USER' in env")
            if 'GMAIL_PASSWORD' in os.environ:
                password = os.environ.get('GMAIL_PASSWORD')
            else:
                raise Exception("No 'GMAIL_PASSWORD' in env")
        except Exception as error:
            print(error)
        finally:
            return sender_email, password

    def _sendEmails(self, weather):
        """The protected method sends weather to emails

        argument:
        weather - <dict>

        """
        sender_email, password = self._setDetails()
        if not sender_email or not password:
            return False

        smtp_server = "smtp.gmail.com"
        port = 587  # For starttls

        # create fancy html message
        message = MIMEMultipart("alternative")
        message["Subject"] = "Currently weather in %s" % self.city
        message["From"] = sender_email
        html = """\
            <html><head><style>
                        table{border-collapse: collapse;
                              border: 1px solid grey;}
                        th, td{border: 1px solid grey;
                               padding: 5px 5px; }
            </style></head><body>%s</body></html>
            """ % tabulate(self._weatherHelper(weather),
                           headers='keys', tablefmt="html")
        html = MIMEText(html, "html")
        message.attach(html)
        # end message

        # Create a secure SSL context
        context = ssl.create_default_context()
        with smtplib.SMTP(smtp_server, port) as server:
            server.ehlo()  # Can be omitted
            server.starttls(context=context)
            server.ehlo()  # Can be omitted
            server.login(sender_email, password)
            for mail in self.emails:
                if validate_email(mail):
                    server.sendmail(sender_email, mail, message.as_string())
                else:
                    print("Email address: %s is not valid!" % mail)

    def setArgs(self, args):
        """The public method sets properties:

        self.city - <str>
        self.get - <str>
        convert mail<str> to self.emails<list>
        self.emails - <list>

        arguments:
        args (city<str>, apikey<str>, |mail|<str>) - <obj>

        """
        try:
            if args.city and args.apikey:
                self.city = args.city
                self.get = "{}&APPID={}".format(
                    self.city, self._setApiKey(args.apikey))
            else:
                raise Exception('No parametrs: city or apikey')
            if args.mail:
                self.emails = args.mail.split(',')
        except Exception as error:
            print(error)
            return False

    def _setApiKey(self, path):
        """The protected method reads and returns apikey from file

        arguments:
        path to file - <str>

        return:
        api key - <str>

        """
        with open(path, "r") as descr:
            apikey = descr.read()
        return apikey.rstrip()

    def goWeather(self):
        """The public method gets weather, prints or sends her to emails"""
        weather = self._getWeather()
        if weather and self.emails:
            self._sendEmails(weather['main'])
        elif weather:
            print("Weather in {}".format(self.city))
            print(tabulate(self._weatherHelper(
                weather['main']), headers='keys'))
        else:
            return False

    def _weatherHelper(self, weather):
        """The protected method does iterable data of humidity, temp, pressure:

        arguments:
        weather - <dict>{key<str>:<str>}

        return:
        weather new - <dict>{key<str>:<list>[<str>]}

        """
        try:
            if weather:
                weather_new = {}
                for k, v in weather.items():
                    if k == 'humidity' or k == 'temp' or k == 'pressure':
                        weather_new[k] = []
                        weather_new[k].append(v)
                return weather_new
        except Exception as error:
            print(error)


def main():
    argsParse = argparse.ArgumentParser()

    argsParse.add_argument('city', type=str, help='city for look out weather')
    argsParse.add_argument('apikey', type=str, help='path to apikey file')
    argsParse.add_argument('--mail', type=str, help='mails to send weather')

    args = argsParse.parse_args()

    parseWeather = ParseWeather()
    parseWeather.setArgs(args)
    parseWeather.goWeather()


if __name__ == "__main__":
    main()
