#!/bin/bash

# coded by Mikalai Ashuika
# email: ashuikam@gmail.com

# The script gets weather from openweather.org
# and prints or sends to emails with mail and ssmtp
# to use it is necessary to install:
# jq mailutils ssmtp curl openssl

# examples:
# send currently weather
# ./parse_weather.sh London apikeyfile bla@domain.net,bla2@domain2.com

# print currently weather:
# ./parse_weather.sh London apikeyfile 

# get parametrs
# sity
if [ -n "$1" ]
then
    city=$1
else
    echo "No parametr city"
    exit 1
fi
# path to apikeyfile
if [ -n "$2" ]
then
    apikeyfile=$2
else
    echo "No parametr path to apikeyfile"
    exit 1
fi
# mails
if [ -n "$3" ]
then
    mails=$3
fi
# read apikeyfile
apikey=$(cat $apikeyfile)
# get weather
weather=$(curl -s "http://api.openweathermap.org/data/2.5/weather?q="$city"&APPID="$apikey)

if [ $? -ne 0 ]
then
    echo "No data from openweathermap.org"
    exit 2
fi
# parse weather with jq
temp=$(echo $weather |jq ."main.temp")
pressure=$(echo $weather |jq ."main.pressure")
humidity=$(echo $weather |jq ."main.humidity")

message="Weather in $city is Temperature=$temp , Pressure=$pressure , humidity=$humidity"
# print or send weather
if [ -z  "$mails" ]
then
    echo $message
else
    echo $message | mail -s "Weather in $city" $mails
    if [ $? -eq 0 ]
    then
        echo "Mail was sent successfully to $mails"
    else
	echo "Mail wasn't sent to $mails"
	exit 3
    fi
fi
exit 0
