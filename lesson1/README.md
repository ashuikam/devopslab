The script PaserWeather.py gets weather from openweathermap.org
prints or sends it to emails. You need an apikey from https://openweathermap.org.

examples:
send weather to emails
./ParserWeather.py [city] [path to apikeyfile] --mail blabla@domain.com,blabla2@domain2.net
print weather
./ParserWeather.py [city] [path to apikeyfile] 

Execute create_env.sh to create an isolated environment and install libs.

Execute source env/bin/activate to activate your environment.

Create environment variables for authorization on your SMTP server with:
export GMAIL_PASSWORD='yourpassword'
export GMAIL_USER=yourname@yourdomain.com

coded by Mikalai Ashuika
email: ashuikam@gmail.com

