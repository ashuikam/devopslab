#!/bin/bash

docker build -t ashuikam/alpine-ansible_2.6.6:1.0 --build-arg ansible_ver=2.6.6 .
docker build -t ashuikam/alpine-ansible_2.7.7:1.0 --build-arg ansible_ver=2.7.7 .

echo "alias a266='docker run --rm -v \$(pwd):/playbook ashuikam/alpine-ansible_2.6.6:1.0'" >> ~/.bashrc
echo "alias a277='docker run --rm -v \$(pwd):/playbook ashuikam/alpine-ansible_2.7.7:1.0'" >> ~/.bashrc
. ~/.bashrc
