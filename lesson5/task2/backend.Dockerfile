FROM tomcat:7

LABEL MAINTAINER "Mikalai Ashuika ashuikam@gmail.com"

# add context to /usr/local/tomcat/webapps

ADD https://tomcat.apache.org/tomcat-7.0-doc/appdev/sample/sample.war\
		/usr/local/tomcat/webapps/
