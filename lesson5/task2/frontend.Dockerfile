FROM  httpd:2.4

LABEL MAINTAINER "Mikalai Ashuika <ashuikam@gmail.com>"

# install curl 
RUN apt-get update && \
    apt-get install curl && \
    apt-get upgrade && \
    apt-get clean
