# Infrastructure AWS for lesson12 (Terraform)  
  
1. You should pass your public key to terraform with env variable:  
  * example: export TF_VAR_public_key="$(cat ~/.ssh/your_key.pub)"  
2. Execute:  
  * cd 0-state-storage  
  * terraform init  
  * terraform apply  
3. Execute:  
  * cd ../1-core-infra  
  * terraform init -backend-config=../backend.hcl  
  * terraform workspace new dev  
  * terraform apply -var-file=tfvars/dev.tfvars  

so for each layer:    

tf-infra-training  
├── 0-state-storage  
│   ├── main.tf  
│   ├── terraform.tfstate  
│   ├── variables.tf  
│   └── versions.tf  
├── 1-core-infra  
│   ├── main.tf  
│   ├── outputs.tf  
│   ├── tfvars  
│   │   ├── dev.tfvars  
│   │   └── prod.tfvars  
│   ├── variables.tf  
│   └── versions.tf  
├── 2-common-resources  
│   ├── main.tf  
│   ├── tfvars  
│   │   ├── dev.tfvars  
│   │   └── prod.tfvars  
│   ├── variables.tf  
│   └── versions.tf  
├── 3-infra-service  
│   ├── main.tf  
│   ├── tfvars  
│   │   ├── dev.tfvars  
│   │   └── prod.tfvars  
│   ├── variables.tf  
│   └── versions.tf  
└── backend.hcl  


