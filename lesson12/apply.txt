grajdanin@nexus:~/training/devopslab/lesson12/tf-infra-training/0-state-storage$ 
grajdanin@nexus:~/training/devopslab/lesson12/tf-infra-training/1-core-infra/.terraform/modules/core.vpc/terraform-aws-modules-terraform-aws-vpc-5358041$ OB
bash: OB: команда не найдена

grajdanin@nexus:~/training/devopslab/lesson12/tf-infra-training/0-state-storage$ terraform apply

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # aws_dynamodb_table.tf_locks will be created
  + resource "aws_dynamodb_table" "tf_locks" {
      + arn              = (known after apply)
      + billing_mode     = "PAY_PER_REQUEST"
      + hash_key         = "LockID"
      + id               = (known after apply)
      + name             = "ashuikam-tf-state-locks"
      + stream_arn       = (known after apply)
      + stream_label     = (known after apply)
      + stream_view_type = (known after apply)

      + attribute {
          + name = "LockID"
          + type = "S"
        }

      + point_in_time_recovery {
          + enabled = (known after apply)
        }

      + server_side_encryption {
          + enabled     = (known after apply)
          + kms_key_arn = (known after apply)
        }
    }

  # aws_s3_bucket.tf_state will be created
  + resource "aws_s3_bucket" "tf_state" {
      + acceleration_status         = (known after apply)
      + acl                         = "private"
      + arn                         = (known after apply)
      + bucket                      = "ashuikam-tf-state"
      + bucket_domain_name          = (known after apply)
      + bucket_regional_domain_name = (known after apply)
      + force_destroy               = false
      + hosted_zone_id              = (known after apply)
      + id                          = (known after apply)
      + region                      = (known after apply)
      + request_payer               = (known after apply)
      + website_domain              = (known after apply)
      + website_endpoint            = (known after apply)

      + server_side_encryption_configuration {
          + rule {
              + apply_server_side_encryption_by_default {
                  + sse_algorithm = "AES256"
                }
            }
        }

      + versioning {
          + enabled    = true
          + mfa_delete = false
        }
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

aws_dynamodb_table.tf_locks: Creating...
aws_s3_bucket.tf_state: Creating...
aws_s3_bucket.tf_state: Creation complete after 7s [id=ashuikam-tf-state]
aws_dynamodb_table.tf_locks: Still creating... [10s elapsed]
aws_dynamodb_table.tf_locks: Creation complete after 10s [id=ashuikam-tf-state-locks]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.

grajdanin@nexus:~/training/devopslab/lesson12/tf-infra-training/1-core-infra$ terraform apply -var-file=tfvars/dev.tfvars 
Acquiring state lock. This may take a few moments...
module.core.data.aws_ami.nat: Refreshing state...

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.core.aws_default_security_group.this[0] will be created
  + resource "aws_default_security_group" "this" {
      + arn                    = (known after apply)
      + description            = (known after apply)
      + egress                 = [
          + {
              + cidr_blocks      = [
                  + "0.0.0.0/0",
                ]
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = false
              + to_port          = 0
            },
        ]
      + id                     = (known after apply)
      + ingress                = [
          + {
              + cidr_blocks      = []
              + description      = ""
              + from_port        = 0
              + ipv6_cidr_blocks = []
              + prefix_list_ids  = []
              + protocol         = "-1"
              + security_groups  = []
              + self             = true
              + to_port          = 0
            },
        ]
      + name                   = (known after apply)
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Environment" = "dev"
          + "Name"        = "lesson12-dev"
          + "Project"     = "lesson12"
        }
      + vpc_id                 = (known after apply)
    }

  # module.http_asg_sg.aws_security_group.this_name_prefix[0] will be created
  + resource "aws_security_group" "this_name_prefix" {
      + arn                    = (known after apply)
      + description            = "Security group with HTTP port open for the elb SG and ssh port open for the jenkins SG"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = (known after apply)
      + name                   = (known after apply)
      + name_prefix            = "http-asg-dev-lesson12-"
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Environment" = "dev"
          + "Name"        = "http-asg-dev-lesson12"
        }
      + vpc_id                 = (known after apply)
    }

  # module.http_asg_sg.aws_security_group_rule.egress_rules[0] will be created
  + resource "aws_security_group_rule" "egress_rules" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "All protocols"
      + from_port                = -1
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = [
          + "::/0",
        ]
      + prefix_list_ids          = []
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = -1
      + type                     = "egress"
    }

  # module.http_asg_sg.aws_security_group_rule.ingress_with_source_security_group_id[0] will be created
  + resource "aws_security_group_rule" "ingress_with_source_security_group_id" {
      + description              = (known after apply)
      + from_port                = (known after apply)
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = []
      + prefix_list_ids          = []
      + protocol                 = (known after apply)
      + security_group_id        = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = (known after apply)
      + type                     = "ingress"
    }

  # module.http_asg_sg.aws_security_group_rule.ingress_with_source_security_group_id[1] will be created
  + resource "aws_security_group_rule" "ingress_with_source_security_group_id" {
      + description              = (known after apply)
      + from_port                = (known after apply)
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = []
      + prefix_list_ids          = []
      + protocol                 = (known after apply)
      + security_group_id        = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = (known after apply)
      + type                     = "ingress"
    }

  # module.core.module.vpc.aws_internet_gateway.this[0] will be created
  + resource "aws_internet_gateway" "this" {
      + id       = (known after apply)
      + owner_id = (known after apply)
      + tags     = {
          + "Environment" = "dev"
          + "Name"        = "lesson12-dev"
          + "Project"     = "lesson12"
        }
      + vpc_id   = (known after apply)
    }

  # module.core.module.vpc.aws_route.public_internet_gateway[0] will be created
  + resource "aws_route" "public_internet_gateway" {
      + destination_cidr_block     = "0.0.0.0/0"
      + destination_prefix_list_id = (known after apply)
      + egress_only_gateway_id     = (known after apply)
      + gateway_id                 = (known after apply)
      + id                         = (known after apply)
      + instance_id                = (known after apply)
      + instance_owner_id          = (known after apply)
      + nat_gateway_id             = (known after apply)
      + network_interface_id       = (known after apply)
      + origin                     = (known after apply)
      + route_table_id             = (known after apply)
      + state                      = (known after apply)

      + timeouts {
          + create = "5m"
        }
    }

  # module.core.module.vpc.aws_route_table.public[0] will be created
  + resource "aws_route_table" "public" {
      + id               = (known after apply)
      + owner_id         = (known after apply)
      + propagating_vgws = (known after apply)
      + route            = (known after apply)
      + tags             = {
          + "Environment" = "dev"
          + "Name"        = "lesson12-dev-public"
          + "Project"     = "lesson12"
        }
      + vpc_id           = (known after apply)
    }

  # module.core.module.vpc.aws_route_table_association.public[0] will be created
  + resource "aws_route_table_association" "public" {
      + id             = (known after apply)
      + route_table_id = (known after apply)
      + subnet_id      = (known after apply)
    }

  # module.core.module.vpc.aws_subnet.public[0] will be created
  + resource "aws_subnet" "public" {
      + arn                             = (known after apply)
      + assign_ipv6_address_on_creation = false
      + availability_zone               = "eu-central-1a"
      + availability_zone_id            = (known after apply)
      + cidr_block                      = "10.0.0.0/28"
      + id                              = (known after apply)
      + ipv6_cidr_block                 = (known after apply)
      + ipv6_cidr_block_association_id  = (known after apply)
      + map_public_ip_on_launch         = true
      + owner_id                        = (known after apply)
      + tags                            = {
          + "Environment" = "dev"
          + "Name"        = "lesson12-dev-public-eu-central-1a"
          + "Project"     = "lesson12"
        }
      + vpc_id                          = (known after apply)
    }

  # module.core.module.vpc.aws_vpc.this[0] will be created
  + resource "aws_vpc" "this" {
      + arn                              = (known after apply)
      + assign_generated_ipv6_cidr_block = false
      + cidr_block                       = "10.0.0.0/24"
      + default_network_acl_id           = (known after apply)
      + default_route_table_id           = (known after apply)
      + default_security_group_id        = (known after apply)
      + dhcp_options_id                  = (known after apply)
      + enable_classiclink               = (known after apply)
      + enable_classiclink_dns_support   = (known after apply)
      + enable_dns_hostnames             = false
      + enable_dns_support               = true
      + id                               = (known after apply)
      + instance_tenancy                 = "default"
      + ipv6_association_id              = (known after apply)
      + ipv6_cidr_block                  = (known after apply)
      + main_route_table_id              = (known after apply)
      + owner_id                         = (known after apply)
      + tags                             = {
          + "Environment" = "dev"
          + "Name"        = "lesson12-dev"
          + "Project"     = "lesson12"
        }
    }

  # module.http_elb_sg.module.sg.aws_security_group.this_name_prefix[0] will be created
  + resource "aws_security_group" "this_name_prefix" {
      + arn                    = (known after apply)
      + description            = "Security group with HTTP port open for everybody (IPv4 CIDR)"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = (known after apply)
      + name                   = (known after apply)
      + name_prefix            = "http-elb-dev-lesson12-"
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Environment" = "dev"
          + "Name"        = "http-elb-dev-lesson12"
        }
      + vpc_id                 = (known after apply)
    }

  # module.http_elb_sg.module.sg.aws_security_group_rule.egress_rules[0] will be created
  + resource "aws_security_group_rule" "egress_rules" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "All protocols"
      + from_port                = -1
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = [
          + "::/0",
        ]
      + prefix_list_ids          = []
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = -1
      + type                     = "egress"
    }

  # module.http_elb_sg.module.sg.aws_security_group_rule.ingress_rules[0] will be created
  + resource "aws_security_group_rule" "ingress_rules" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "HTTP"
      + from_port                = 80
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = []
      + prefix_list_ids          = []
      + protocol                 = "tcp"
      + security_group_id        = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 80
      + type                     = "ingress"
    }

  # module.http_elb_sg.module.sg.aws_security_group_rule.ingress_with_self[0] will be created
  + resource "aws_security_group_rule" "ingress_with_self" {
      + description              = "Ingress Rule"
      + from_port                = -1
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = []
      + prefix_list_ids          = []
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + self                     = true
      + source_security_group_id = (known after apply)
      + to_port                  = -1
      + type                     = "ingress"
    }

  # module.http_ssh_jenkins_sg.module.sg.aws_security_group.this_name_prefix[0] will be created
  + resource "aws_security_group" "this_name_prefix" {
      + arn                    = (known after apply)
      + description            = "Security group with HTTP(8080) and SSH port open just for everybody (IPv4 CIDR)"
      + egress                 = (known after apply)
      + id                     = (known after apply)
      + ingress                = (known after apply)
      + name                   = (known after apply)
      + name_prefix            = "http-jenkins-dev-lesson12-"
      + owner_id               = (known after apply)
      + revoke_rules_on_delete = false
      + tags                   = {
          + "Environment" = "dev"
          + "Name"        = "http-jenkins-dev-lesson12"
        }
      + vpc_id                 = (known after apply)
    }

  # module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.egress_rules[0] will be created
  + resource "aws_security_group_rule" "egress_rules" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "All protocols"
      + from_port                = -1
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = [
          + "::/0",
        ]
      + prefix_list_ids          = []
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = -1
      + type                     = "egress"
    }

  # module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.ingress_rules[0] will be created
  + resource "aws_security_group_rule" "ingress_rules" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "HTTP"
      + from_port                = 8080
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = []
      + prefix_list_ids          = []
      + protocol                 = "tcp"
      + security_group_id        = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 8080
      + type                     = "ingress"
    }

  # module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.ingress_rules[1] will be created
  + resource "aws_security_group_rule" "ingress_rules" {
      + cidr_blocks              = [
          + "0.0.0.0/0",
        ]
      + description              = "SSH"
      + from_port                = 22
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = []
      + prefix_list_ids          = []
      + protocol                 = "tcp"
      + security_group_id        = (known after apply)
      + self                     = false
      + source_security_group_id = (known after apply)
      + to_port                  = 22
      + type                     = "ingress"
    }

  # module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.ingress_with_self[0] will be created
  + resource "aws_security_group_rule" "ingress_with_self" {
      + description              = "Ingress Rule"
      + from_port                = -1
      + id                       = (known after apply)
      + ipv6_cidr_blocks         = []
      + prefix_list_ids          = []
      + protocol                 = "-1"
      + security_group_id        = (known after apply)
      + self                     = true
      + source_security_group_id = (known after apply)
      + to_port                  = -1
      + type                     = "ingress"
    }

Plan: 20 to add, 0 to change, 0 to destroy.

Do you want to perform these actions in workspace "dev"?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.core.module.vpc.aws_vpc.this[0]: Creating...
module.core.module.vpc.aws_vpc.this[0]: Creation complete after 5s [id=vpc-06169fdc34c4130d9]
module.core.module.vpc.aws_route_table.public[0]: Creating...
module.core.module.vpc.aws_internet_gateway.this[0]: Creating...
module.core.module.vpc.aws_subnet.public[0]: Creating...
module.http_ssh_jenkins_sg.module.sg.aws_security_group.this_name_prefix[0]: Creating...
module.http_asg_sg.aws_security_group.this_name_prefix[0]: Creating...
module.core.aws_default_security_group.this[0]: Creating...
module.http_elb_sg.module.sg.aws_security_group.this_name_prefix[0]: Creating...
module.core.module.vpc.aws_route_table.public[0]: Creation complete after 2s [id=rtb-0b8fb7a9ed9220d49]
module.core.module.vpc.aws_subnet.public[0]: Creation complete after 2s [id=subnet-094f0d72461f39dfa]
module.core.module.vpc.aws_route_table_association.public[0]: Creating...
module.core.module.vpc.aws_internet_gateway.this[0]: Creation complete after 2s [id=igw-02d544082a6ebcc50]
module.core.module.vpc.aws_route.public_internet_gateway[0]: Creating...
module.core.module.vpc.aws_route_table_association.public[0]: Creation complete after 1s [id=rtbassoc-0dc4e567e32d3195c]
module.http_ssh_jenkins_sg.module.sg.aws_security_group.this_name_prefix[0]: Creation complete after 3s [id=sg-0f547d117bed4e60e]
module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.ingress_with_self[0]: Creating...
module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.egress_rules[0]: Creating...
module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.ingress_rules[1]: Creating...
module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.ingress_rules[0]: Creating...
module.core.module.vpc.aws_route.public_internet_gateway[0]: Creation complete after 1s [id=r-rtb-0b8fb7a9ed9220d491080289494]
module.http_elb_sg.module.sg.aws_security_group.this_name_prefix[0]: Creation complete after 3s [id=sg-06651727cec6a428d]
module.http_asg_sg.aws_security_group.this_name_prefix[0]: Creation complete after 3s [id=sg-06e10b7dda64a548b]
module.http_elb_sg.module.sg.aws_security_group_rule.ingress_rules[0]: Creating...
module.http_elb_sg.module.sg.aws_security_group_rule.ingress_with_self[0]: Creating...
module.http_asg_sg.aws_security_group_rule.ingress_with_source_security_group_id[1]: Creating...
module.http_asg_sg.aws_security_group_rule.ingress_with_source_security_group_id[0]: Creating...
module.http_elb_sg.module.sg.aws_security_group_rule.egress_rules[0]: Creating...
module.core.aws_default_security_group.this[0]: Creation complete after 3s [id=sg-02ebf84b6d819b7cf]
module.http_asg_sg.aws_security_group_rule.egress_rules[0]: Creating...
module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.ingress_with_self[0]: Creation complete after 1s [id=sgrule-2855251456]
module.http_asg_sg.aws_security_group_rule.ingress_with_source_security_group_id[1]: Creation complete after 1s [id=sgrule-23343550]
module.http_elb_sg.module.sg.aws_security_group_rule.ingress_rules[0]: Creation complete after 1s [id=sgrule-3781775724]
module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.egress_rules[0]: Creation complete after 2s [id=sgrule-632429030]
module.http_elb_sg.module.sg.aws_security_group_rule.ingress_with_self[0]: Creation complete after 3s [id=sgrule-3864288243]
module.http_asg_sg.aws_security_group_rule.ingress_with_source_security_group_id[0]: Creation complete after 3s [id=sgrule-4097281046]
module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.ingress_rules[1]: Creation complete after 4s [id=sgrule-3652650325]
module.http_elb_sg.module.sg.aws_security_group_rule.egress_rules[0]: Creation complete after 4s [id=sgrule-336296075]
module.http_asg_sg.aws_security_group_rule.egress_rules[0]: Creation complete after 4s [id=sgrule-1591290050]
module.http_ssh_jenkins_sg.module.sg.aws_security_group_rule.ingress_rules[0]: Creation complete after 5s [id=sgrule-3590874835]

Apply complete! Resources: 20 added, 0 changed, 0 destroyed.                                                                                                                                                      
Releasing state lock. This may take a few moments...

Outputs:                                                                                                                                                                                                          
                                                                                                                                                                                                                  
public_subnets = [                                                                                                                                                                                                
  "subnet-094f0d72461f39dfa",                                                                                                                                                                                     
]                                                                                                                                                                                                                 
security_group_id = sg-06651727cec6a428d

grajdanin@nexus:~/training/devopslab/lesson12/tf-infra-training/2-common-resources$ terraform apply -var-file=tfvars/dev.tfvars 
data.aws_security_groups.jenkins: Refreshing state...
data.aws_subnet.subnets: Refreshing state...

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.ec2.aws_instance.this[0] will be created
  + resource "aws_instance" "this" {
      + ami                          = "ami-0b418580298265d5c"
      + arn                          = (known after apply)
      + associate_public_ip_address  = (known after apply)
      + availability_zone            = (known after apply)
      + cpu_core_count               = (known after apply)
      + cpu_threads_per_core         = (known after apply)
      + disable_api_termination      = false
      + ebs_optimized                = false
      + get_password_data            = false
      + host_id                      = (known after apply)
      + id                           = (known after apply)
      + instance_state               = (known after apply)
      + instance_type                = "t2.micro"
      + ipv6_address_count           = (known after apply)
      + ipv6_addresses               = (known after apply)
      + key_name                     = "lesson12-dev-key"
      + monitoring                   = false
      + network_interface_id         = (known after apply)
      + password_data                = (known after apply)
      + placement_group              = (known after apply)
      + primary_network_interface_id = (known after apply)
      + private_dns                  = (known after apply)
      + private_ip                   = (known after apply)
      + public_dns                   = (known after apply)
      + public_ip                    = (known after apply)
      + security_groups              = (known after apply)
      + source_dest_check            = true
      + subnet_id                    = "subnet-094f0d72461f39dfa"
      + tags                         = {
          + "Environment" = "dev"
          + "Name"        = "jenkins-lesson12-dev"
        }
      + tenancy                      = "default"
      + volume_tags                  = {
          + "Name" = "jenkins-lesson12-dev"
        }
      + vpc_security_group_ids       = [
          + "sg-0f547d117bed4e60e",
        ]

      + credit_specification {
          + cpu_credits = "standard"
        }

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + snapshot_id           = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + ephemeral_block_device {
          + device_name  = (known after apply)
          + no_device    = (known after apply)
          + virtual_name = (known after apply)
        }

      + network_interface {
          + delete_on_termination = (known after apply)
          + device_index          = (known after apply)
          + network_interface_id  = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + kms_key_id            = (known after apply)
          + volume_id             = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # module.key_pair.aws_key_pair.this[0] will be created
  + resource "aws_key_pair" "this" {
      + fingerprint = (known after apply)
      + id          = (known after apply)
      + key_name    = "lesson12-dev-key"
      + key_pair_id = (known after apply)
      + public_key  = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDVmoJ+wlQP02K90KKE2TzYu2rm5dL3MiY1uEYrQlcOIngW8XuCTm/+8jVm38yhpvIPhsP+KlB1x5JNViV6LOBDNJyesCk4WXLZssflfC9OprkPmnf1w+sPkvE7iQUESvOKB9mMkIpyKDHu1DeMGSUa7n+zGs3lt3lip75DErXjqb6yEauyjOsy9sT4FTRnVYi58PHJfMz3i4OD8yQwfX6Lj4wHJfUE19Lp9uLttFECGpeehdC++W2tOL7KET0Q3pVTTjjFC5GaJ0D5iDi0HRKEAg/56OXojKJFMncXorTY1SPQuf9qxzMWa7+jZsZ4ggTTOMlGlUDA7X2B5ipjJ+kN grajdanin@nexus"
    }

Plan: 2 to add, 0 to change, 0 to destroy.

Do you want to perform these actions in workspace "dev"?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.key_pair.aws_key_pair.this[0]: Creating...
module.key_pair.aws_key_pair.this[0]: Creation complete after 1s [id=lesson12-dev-key]
module.ec2.aws_instance.this[0]: Creating...
module.ec2.aws_instance.this[0]: Still creating... [10s elapsed]
module.ec2.aws_instance.this[0]: Still creating... [20s elapsed]
module.ec2.aws_instance.this[0]: Still creating... [30s elapsed]
module.ec2.aws_instance.this[0]: Creation complete after 35s [id=i-05b19f7275342dcad]

Apply complete! Resources: 2 added, 0 changed, 0 destroyed.                                                                                                                                                       
Releasing state lock. This may take a few moments...


grajdanin@nexus:~/training/devopslab/lesson12/tf-infra-training/3-infra-service$ terraform apply -var-file=tfvars/dev.tfvars 
Acquiring state lock. This may take a few moments...
data.aws_security_groups.elb: Refreshing state...
data.aws_security_groups.asg: Refreshing state...
data.aws_subnet.subnets: Refreshing state...

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  # module.asg.aws_autoscaling_group.this[0] will be created
  + resource "aws_autoscaling_group" "this" {
      + arn                       = (known after apply)
      + availability_zones        = (known after apply)
      + default_cooldown          = 300
      + desired_capacity          = 3
      + enabled_metrics           = [
          + "GroupDesiredCapacity",
          + "GroupInServiceInstances",
          + "GroupMaxSize",
          + "GroupMinSize",
          + "GroupPendingInstances",
          + "GroupStandbyInstances",
          + "GroupTerminatingInstances",
          + "GroupTotalInstances",
        ]
      + force_delete              = false
      + health_check_grace_period = 300
      + health_check_type         = "EC2"
      + id                        = (known after apply)
      + launch_configuration      = (known after apply)
      + load_balancers            = (known after apply)
      + max_size                  = 6
      + metrics_granularity       = "1Minute"
      + min_elb_capacity          = 0
      + min_size                  = 2
      + name                      = (known after apply)
      + name_prefix               = "lesson12-asg-"
      + protect_from_scale_in     = false
      + service_linked_role_arn   = (known after apply)
      + tags                      = [
          + {
              + "key"                 = "Name"
              + "propagate_at_launch" = "true"
              + "value"               = "asg-dev-lesson12"
            },
          + {
              + "key"                 = "Environment"
              + "propagate_at_launch" = "true"
              + "value"               = "dev"
            },
        ]
      + target_group_arns         = (known after apply)
      + termination_policies      = [
          + "Default",
        ]
      + vpc_zone_identifier       = [
          + "subnet-094f0d72461f39dfa",
        ]
      + wait_for_capacity_timeout = "0"
    }

  # module.asg.aws_launch_configuration.this[0] will be created
  + resource "aws_launch_configuration" "this" {
      + arn                         = (known after apply)
      + associate_public_ip_address = false
      + ebs_optimized               = false
      + enable_monitoring           = true
      + id                          = (known after apply)
      + image_id                    = "ami-0b418580298265d5c"
      + instance_type               = "t2.micro"
      + key_name                    = "lesson12-dev-key"
      + name                        = (known after apply)
      + name_prefix                 = "lc--dev-lesson12-"
      + placement_tenancy           = "default"
      + security_groups             = [
          + "sg-06e10b7dda64a548b",
        ]
      + user_data                   = "b858cb282617fb0956d960215c8e84d1ccf909c6"

      + ebs_block_device {
          + delete_on_termination = (known after apply)
          + device_name           = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + no_device             = (known after apply)
          + snapshot_id           = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }

      + root_block_device {
          + delete_on_termination = (known after apply)
          + encrypted             = (known after apply)
          + iops                  = (known after apply)
          + volume_size           = (known after apply)
          + volume_type           = (known after apply)
        }
    }

  # module.elb.module.elb.aws_elb.this will be created
  + resource "aws_elb" "this" {
      + arn                         = (known after apply)
      + availability_zones          = (known after apply)
      + connection_draining         = false
      + connection_draining_timeout = 300
      + cross_zone_load_balancing   = true
      + dns_name                    = (known after apply)
      + id                          = (known after apply)
      + idle_timeout                = 60
      + instances                   = (known after apply)
      + internal                    = false
      + name                        = "elb-dev-lesson12"
      + security_groups             = [
          + "sg-06651727cec6a428d",
        ]
      + source_security_group       = (known after apply)
      + source_security_group_id    = (known after apply)
      + subnets                     = [
          + "subnet-094f0d72461f39dfa",
        ]
      + tags                        = {
          + "Environment" = "dev"
          + "Name"        = "elb-dev-lesson12"
        }
      + zone_id                     = (known after apply)

      + health_check {
          + healthy_threshold   = 2
          + interval            = 30
          + target              = "HTTP:8080/"
          + timeout             = 5
          + unhealthy_threshold = 2
        }

      + listener {
          + instance_port     = 8080
          + instance_protocol = "HTTP"
          + lb_port           = 80
          + lb_protocol       = "HTTP"
        }
    }

Plan: 3 to add, 0 to change, 0 to destroy.

Do you want to perform these actions in workspace "dev"?
  Terraform will perform the actions described above.
  Only 'yes' will be accepted to approve.

  Enter a value: yes

module.asg.aws_launch_configuration.this[0]: Creating...
module.elb.module.elb.aws_elb.this: Creating...
module.asg.aws_launch_configuration.this[0]: Creation complete after 2s [id=lc--dev-lesson12-20200207132745629100000001]
module.elb.module.elb.aws_elb.this: Creation complete after 6s [id=elb-dev-lesson12]
module.asg.aws_autoscaling_group.this[0]: Creating...
module.asg.aws_autoscaling_group.this[0]: Creation complete after 2s [id=lesson12-asg-20200207132750691100000002]

Apply complete! Resources: 3 added, 0 changed, 0 destroyed.                                                                                                                                                       
Releasing state lock. This may take a few moments...

