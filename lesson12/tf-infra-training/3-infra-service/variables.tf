variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "eu-central-1"
}

variable "project_name" {
  description = "AWS project name."
}

variable "image_id" {
  description = "AWS AMI id"
}

variable "instance_type" {
  description = "AWS instance type"
}

variable "instance_count" {}

variable "listen_to" {}

variable "check" {}

variable "ag_min_size" {}

variable "ag_max_size" {}

variable "ag_desired_capacity" {}

variable "ag_wait_for_capacity_timeout" {}