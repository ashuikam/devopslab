terraform {
  required_version = ">= 0.12"
  required_providers {
    aws = "~> 2.48"
    null = "~> 2.1"
    random = "~> 2.2"

  }
}
