provider "aws" {
  region = var.aws_region
}

data "aws_security_groups" "elb" {
  filter {
    name   = "tag:Name"
    values = ["http-elb-${terraform.workspace}-${var.project_name}"]
  }
}

data "aws_security_groups" "asg" {
  filter {
    name   = "tag:Name"
    values = ["http-asg-${terraform.workspace}-${var.project_name}"]
  }
}

data "aws_subnet" "subnets" {
  filter {
    name   = "tag:Name"
    values = ["${var.project_name}*"]
  }
  filter {
    name = "tag:Environment"
    values = [terraform.workspace]
  }
}

module "elb" {
  source  = "terraform-aws-modules/elb/aws"
  version = "~> 2.0"

  name = "elb-${terraform.workspace}-${var.project_name}"

  subnets         = [data.aws_subnet.subnets.id]
  security_groups = data.aws_security_groups.elb.ids
  internal        = false

  listener = [var.listen_to]

  health_check = var.check
 
  tags = {
    Environment = terraform.workspace
  }
}

module "asg" {
  source  = "terraform-aws-modules/autoscaling/aws"
  version = "~> 3.0"

  name = "asg-${terraform.workspace}-${var.project_name}"

  # Launch configuration
  lc_name = "lc--${terraform.workspace}-${var.project_name}"

  image_id        = var.image_id
  instance_type   = var.instance_type
  key_name        = "${var.project_name}-${terraform.workspace}-key"
  security_groups = data.aws_security_groups.asg.ids

  # Auto scaling group
  asg_name                  = "${var.project_name}-asg"
  vpc_zone_identifier       = [data.aws_subnet.subnets.id]
  health_check_type         = "EC2"
  min_size                  = var.ag_min_size
  max_size                  = var.ag_max_size
  desired_capacity          = var.ag_desired_capacity
  wait_for_capacity_timeout = var.ag_wait_for_capacity_timeout
  load_balancers            = [module.elb.this_elb_id]

  tags = [
    {
      key = "Environment"
      value = terraform.workspace
      propagate_at_launch = true
    },
  ]
}

terraform {
  backend "s3" {
    workspace_key_prefix = "service"
  }
}
