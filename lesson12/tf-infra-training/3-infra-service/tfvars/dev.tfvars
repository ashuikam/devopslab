aws_region = "eu-central-1"

project_name = "lesson12"

instance_type = "t2.micro"

image_id = "ami-0b418580298265d5c"

instance_count = 1

listen_to = {
 instance_port     = "8080"
 instance_protocol = "HTTP"
 lb_port           = "80"
 lb_protocol       = "HTTP"
}

check = {
  target              = "HTTP:8080/"
  interval            = 30
  healthy_threshold   = 2
  unhealthy_threshold = 2
  timeout             = 5
}

ag_min_size = 2

ag_max_size = 6

ag_desired_capacity = 3

ag_wait_for_capacity_timeout = 0