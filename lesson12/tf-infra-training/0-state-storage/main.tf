provider "aws" {
    region = var.aws_region
}

# Amazon S3 Bucket

resource "aws_s3_bucket" "tf_state" {
    bucket = var.bucket

    # removal protection
		#lifecycle {
		#  prevent_destroy = true
		#}

    versioning {
      enabled = true
    }
    server_side_encryption_configuration {
      rule {
        apply_server_side_encryption_by_default {
          sse_algorithm = var.sse_algorithm
        }
      }
    }
}

# locks
resource "aws_dynamodb_table" "tf_locks" {
    name = var.locks_name
    billing_mode = var.billing_mode
    hash_key = var.hash_key

    attribute {
        name = var.hash_key
        type = "S"
    }
}
