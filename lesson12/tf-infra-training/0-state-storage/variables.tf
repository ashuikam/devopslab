variable "aws_region" {
  description = "AWS region to launch servers."
  default     = "eu-central-1"
}

variable "bucket" {
  default = "ashuikam-tf-state"
}

variable "locks_name" {
  default = "ashuikam-tf-state-locks"
}

variable "billing_mode" {
  default = "PAY_PER_REQUEST"
}

variable "sse_algorithm" {
  default = "AES256"
}

variable "hash_key" {
  default = "LockID"
}