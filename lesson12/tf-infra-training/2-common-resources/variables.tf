variable "aws_region" {
  description = "AWS region to launch servers."
}

variable "project_name" {
  description = "AWS project name."
}

variable "image_id" {
  description = "AWS AMI id"
}

variable "instance_type" {
  description = "AWS instance type"
}

variable "instance_count" {}

variable "key_name" {
  default     = "trainaws"
}

variable "public_key" {
  type        = string
}

variable "ec2_instance_name" {}

variable "ec2_instance_monitoring" {}
