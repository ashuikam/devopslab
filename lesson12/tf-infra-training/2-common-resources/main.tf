provider "aws" {
  region = var.aws_region
}

module "key_pair" {
  source = "terraform-aws-modules/key-pair/aws"

  key_name = "${var.project_name}-${terraform.workspace}-key"
  public_key = var.public_key
}

data "aws_security_groups" "jenkins" {
  filter {
    name = "tag:Name"
    values = ["http-jenkins-${terraform.workspace}-${var.project_name}"]
  }
}

data "aws_subnet" "subnets" {
  filter {
    name = "tag:Name"
    values = ["${var.project_name}*"]
  }
  filter {
    name = "tag:Environment"
    values = [terraform.workspace]
  }
}

module "ec2" {
  source = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 2.0"

  name = "${var.ec2_instance_name}-${var.project_name}-${terraform.workspace}"
  instance_count = var.instance_count

  ami = var.image_id
  instance_type = var.instance_type
  key_name = module.key_pair.this_key_pair_key_name
  monitoring = var.ec2_instance_monitoring
  vpc_security_group_ids = data.aws_security_groups.jenkins.ids
  subnet_id = data.aws_subnet.subnets.id

  tags = {
    Environment = terraform.workspace
  }
}

terraform {
  backend "s3" {
    workspace_key_prefix = "res"
  }
}