aws_region = "eu-central-1"

project_name = "lesson12"

instance_type = "t2.micro"

image_id = "ami-0b418580298265d5c"

instance_count = 1

ec2_instance_name = "jenkins"

ec2_instance_monitoring = false
