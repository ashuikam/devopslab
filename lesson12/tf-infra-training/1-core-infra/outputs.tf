output "security_group_id" {
  value = module.http_elb_sg.this_security_group_id
}

output "public_subnets" {
  value = module.core.public_subnets
}