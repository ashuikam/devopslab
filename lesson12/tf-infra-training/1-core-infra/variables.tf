variable "aws_region" {
  description = "AWS region to launch servers."
}

variable "project_name" {
  description = "AWS project name."
}

variable "public_subnets" {
  description = "AWS public subnets (CIDRs) type: list"
}

variable "vpc_cidr" {
  description = "AWS VPC CIDR type: string"
}

variable "all_nets" {
  description = "CIDR all networks type:list"
  default = ["0.0.0.0/0"]
}

variable "ingress_http_rule" {
  default = "http-8080-tcp"
}

variable "ingress_ssh_rule" {
  default = "ssh-tcp"
}

variable "egress_all_rule" {
  default = "all-all"
}

variable "ld_core_nat_gw" {
  default = "false"
}