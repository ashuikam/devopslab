provider "aws" {
  region = var.aws_region
}

module "core" {
  source = "github.com/lean-delivery/tf-module-awscore"

  project = var.project_name
  environment = terraform.workspace
  availability_zones = ["${var.aws_region}a", "${var.aws_region}b"]
  vpc_cidr = var.vpc_cidr
  public_subnets = var.public_subnets

  enable_nat_gateway = var.ld_core_nat_gw

  tags = {
    Environment = terraform.workspace
  }
}


# Security groups section begin

module "http_elb_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-80"
  version = "~> 3.0"

  name = "http-elb-${terraform.workspace}-${var.project_name}"

  description = "Security group with HTTP port open for everybody (IPv4 CIDR)"
  vpc_id = module.core.vpc_id

  ingress_cidr_blocks = var.all_nets

  tags = {
    Environment = terraform.workspace
  }
}

module "http_asg_sg" {
  source = "terraform-aws-modules/security-group/aws"
  version = "~> 3.0"

  name = "http-asg-${terraform.workspace}-${var.project_name}"
  description = "Security group with HTTP port open for the elb SG and ssh port open for the jenkins SG"
  vpc_id = module.core.vpc_id

  ingress_cidr_blocks = var.public_subnets

  ingress_with_source_security_group_id = [
    {
      rule = var.ingress_http_rule
      source_security_group_id = module.http_elb_sg.this_security_group_id
    },
    {
      rule = var.ingress_ssh_rule
      source_security_group_id = module.http_ssh_jenkins_sg.this_security_group_id
    },
  ]

  egress_rules = [var.egress_all_rule]

  tags = {
    Environment = terraform.workspace
  }
}

module "http_ssh_jenkins_sg" {
  source = "terraform-aws-modules/security-group/aws//modules/http-8080"
  version = "~> 3.0"

  name = "http-jenkins-${terraform.workspace}-${var.project_name}"
  description = "Security group with HTTP(8080) and SSH port open just for everybody (IPv4 CIDR)"
  vpc_id = module.core.vpc_id

  ingress_cidr_blocks = var.all_nets

  ingress_rules = [var.ingress_ssh_rule]

  tags = {
    Environment = terraform.workspace
  }
}

# Security groups section end

terraform {
  backend "s3" {
    workspace_key_prefix = "core"
  }
}