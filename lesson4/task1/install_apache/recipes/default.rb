#
# Cookbook:: install_apache
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

psw = `openssl passwd -1 apache`

user 'apache' do
  comment 'apache user'
  home '/home/apache'
  shell '/bin/bash'
  password 'apache'
end

package "httpd" do
  action :install
end

service "httpd" do
  action [:enable, :start]
end

template '/etc/httpd/conf/httpd.conf' do
  source 'httpd.erb'
  owner 'root'
  group 'root'
  mode '0644'
  variables(port: node['httpd']['port'])
  notifies :restart, 'service[httpd]', :immediately
end

template '/var/www/html/index.html' do
  source 'index.erb'
  owner 'root'
  group 'root'
  mode '0644'
  notifies :restart, 'service[httpd]', :immediately
end

