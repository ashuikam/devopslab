# InSpec test for recipe install_apache::default

# The InSpec reference, with examples and extensive documentation, can be
# found at https://www.inspec.io/docs/reference/resources/

unless os.windows?
  # This is an example test, replace with your own test.
  describe user('root'), :skip do
    it { should exist }
  end
end

# This is an example test, replace it with your own test.

describe package('httpd') do
  it { should be_installed }
end

describe service('httpd') do
  it { should be_installed }
  it { should be_enabled }
  it { should be_running }
end

describe port(8000), :skip do
  it { should_not be_listening }
end

describe http('localhost:8000') do
  its('status') { should eq 200 }
  its('headers.Content-Type') { should include 'text/html' }
end
