#!/bin/bash

web='my-ingress.loc'
org='epam-training'
file='inserv'

# generate sert, key and secret config
# if you have a default fake certificate
# you may comment that block
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout $file.key -out $file.crt -subj "/CN=$web/O=$org"
kubectl create secret tls $org --key $file.key --cert $file.crt -o yaml --dry-run > "secret_$org.yml"
kubectl apply -f secret_$org.yml

# configure ingress
kubectl apply -f ingress.yml

# create internal deploy and services
kubectl apply -f dep_svc_nginx.yml
kubectl apply -f dep_svc_tomcat.yml

