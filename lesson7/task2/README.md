Ingress task

execute ingress.sh
add "<your ip> my-ingress.loc" to /etc/hosts

examples of test urls for your web browser or curl:

http://my-ingress.loc/
http://my-ingress.loc/sample
https://my-ingress.loc/
https://my-ingress.loc/sample
